module.exports = {
  pathPrefix: `/gatsby-site`,
  siteMetadata: {
    title: `Gatsby Default Starter`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
